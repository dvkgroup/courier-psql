package run

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"gitlab.com/dvkgroup/courier-psql/geo"
	cservice "gitlab.com/dvkgroup/courier-psql/module/courier/service"
	cstorage "gitlab.com/dvkgroup/courier-psql/module/courier/storage"
	"gitlab.com/dvkgroup/courier-psql/module/courierfacade/controller"
	cfservice "gitlab.com/dvkgroup/courier-psql/module/courierfacade/service"
	oservice "gitlab.com/dvkgroup/courier-psql/module/order/service"
	ostorage "gitlab.com/dvkgroup/courier-psql/module/order/storage"
	"gitlab.com/dvkgroup/courier-psql/router"
	"gitlab.com/dvkgroup/courier-psql/server"
	"gitlab.com/dvkgroup/courier-psql/workers/order"
	"log"
	"net/http"
	"os"
)

type App struct {
}

func NewApp() *App {
	return &App{}
}

func (a *App) Run() error {
	// получение хоста и порта
	host := os.Getenv("DB_HOST")
	port := os.Getenv("DB_PORT")
	user := os.Getenv("DB_USER")
	pass := os.Getenv("DB_PASSWORD")

	// инициализация клиента postgress
	connStr := fmt.Sprintf("host=%s port=%s user=%s password=%s sslmode=disable",
		host, port, user, pass)
	db, err := sqlx.Connect("postgres", connStr)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	// парсим polygon.js
	polygons, err := geo.ParsePolygonJS("./public/js/polygon.js")
	if err != nil {
		return err
	}

	// инициализация разрешенной зоны
	allowedZone := geo.NewAllowedZone(polygons["mainPolygon"])
	// инициализация запрещенных зон
	disAllowedZones := []geo.PolygonChecker{geo.NewDisAllowedZone1(polygons["noOrdersPolygon1"]),
		geo.NewDisAllowedZone2(polygons["noOrdersPolygon2"])}

	// инициализация хранилища заказов
	orderStorage := ostorage.NewOrderStorage(db)
	// инициализация сервиса заказов
	orderService := oservice.NewOrderService(orderStorage, allowedZone, disAllowedZones)

	orderGenerator := order.NewOrderGenerator(orderService)
	orderGenerator.Run()

	oldOrderCleaner := order.NewOrderCleaner(orderService)
	oldOrderCleaner.Run()

	// инициализация хранилища курьеров
	courierStorage := cstorage.NewCourierStorage(db)
	// инициализация сервиса курьеров
	courierSevice := cservice.NewCourierService(courierStorage, allowedZone, disAllowedZones)

	// инициализация фасада сервиса курьеров
	courierFacade := cfservice.NewCourierFacade(courierSevice, orderService)

	// инициализация контроллера курьеров
	courierController := controller.NewCourierController(courierFacade)

	// инициализация роутера
	routes := router.NewRouter(courierController)
	// инициализация сервера
	r := server.NewHTTPServer()
	// инициализация группы роутов
	api := r.Group("/api")
	// инициализация роутов
	routes.CourierAPI(api)

	mainRoute := r.Group("/")

	routes.Swagger(mainRoute)
	// инициализация статических файлов
	r.NoRoute(gin.WrapH(http.FileServer(http.Dir("public"))))

	// запуск сервера
	//serverPort := os.Getenv("SERVER_PORT")

	if os.Getenv("ENV") == "prod" {
		certFile := "/app/certs/cert.pem"
		keyFile := "/app/certs/private.pem"
		return r.RunTLS(":443", certFile, keyFile)
	}

	return r.Run()
}
