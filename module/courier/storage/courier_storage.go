package storage

import (
	"context"
	"github.com/jmoiron/sqlx"
	"gitlab.com/dvkgroup/courier-psql/module/courier/models"
)

//go:generate go run github.com/vektra/mockery/v2@v2.20.0 --name CourierStorager
type CourierStorager interface {
	Save(ctx context.Context, courier models.Courier) error // сохранить курьера по ключу courier
	GetOne(ctx context.Context) (*models.Courier, error)    // получить курьера по ключу courier
}

type CourierStorage struct {
	db *sqlx.DB
}

func NewCourierStorage(db *sqlx.DB) CourierStorager {
	return &CourierStorage{db: db}
}

func (c CourierStorage) Save(ctx context.Context, courier models.Courier) error {
	query := `INSERT INTO couriers VALUES ($1, $2, $3, $4) ON CONFLICT (id) DO UPDATE SET score = $2, lat = $3, lng = $4`

	courierId := 0

	_, err := c.db.Exec(query, courierId, courier.Score, courier.Location.Lat, courier.Location.Lng)
	if err != nil {
		return err
	}

	return nil
}

func (c CourierStorage) GetOne(ctx context.Context) (*models.Courier, error) {
	res := &models.Courier{}

	query := `SELECT id, score, lat as "location.lat", lng as "location.lng" FROM couriers LIMIT 1`

	err := c.db.Get(res, query)
	if err != nil {
		return nil, err
	}

	return res, nil
}
