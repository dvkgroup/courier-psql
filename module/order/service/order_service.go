package service

import (
	"context"
	"gitlab.com/dvkgroup/courier-psql/geo"
	"gitlab.com/dvkgroup/courier-psql/module/order/models"
	"gitlab.com/dvkgroup/courier-psql/module/order/storage"
	"math"
	"math/rand"
	"time"
)

const (
	minDeliveryPrice = 100.00
	maxDeliveryPrice = 500.00

	maxOrderPrice = 3000.00
	minOrderPrice = 1000.00

	orderMaxAge = 2 * time.Minute
)

//go:generate go run github.com/vektra/mockery/v2@v2.20.0 --name Orderer
type Orderer interface {
	GetByRadius(ctx context.Context, lng, lat, radius float64, unit string) ([]models.Order, error) // возвращает заказы через метод storage.GetByRadius
	Save(ctx context.Context, order models.Order) error                                             // сохраняет заказ через метод storage.Save с заданным временем жизни OrderMaxAge
	GetCount(ctx context.Context) (int, error)                                                      // возвращает количество заказов через метод storage.GetCount
	RemoveOldOrders(ctx context.Context) error                                                      // удаляет старые заказы через метод storage.RemoveOldOrders с заданным временем жизни OrderMaxAge
	GenerateOrder(ctx context.Context) error                                                        // генерирует заказ в случайной точке из разрешенной зоны, с уникальным id, ценой и ценой доставки
	Delivery(ctx context.Context, order models.Order) error                                         // оформляем доставку
}

// OrderService реализация интерфейса Orderer
// в нем должны быть методы GetByRadius, Save, GetCount, RemoveOldOrders, GenerateOrder
// данный сервис отвечает за работу с заказами
type OrderService struct {
	storage       storage.OrderStorager
	allowedZone   geo.PolygonChecker
	disabledZones []geo.PolygonChecker
}

func NewOrderService(storage storage.OrderStorager, allowedZone geo.PolygonChecker, disallowedZone []geo.PolygonChecker) Orderer {
	return &OrderService{storage: storage, allowedZone: allowedZone, disabledZones: disallowedZone}
}

func (o *OrderService) GetByRadius(ctx context.Context, lng, lat, radius float64, unit string) ([]models.Order, error) {
	return o.storage.GetByRadius(ctx, lng, lat, radius, unit)
}

func (o *OrderService) Save(ctx context.Context, order models.Order) error {
	return o.storage.Save(ctx, order, orderMaxAge)
}

func (o *OrderService) GetCount(ctx context.Context) (int, error) {
	return o.storage.GetCount(ctx)
}

func (o *OrderService) RemoveOldOrders(ctx context.Context) error {
	return o.storage.RemoveOldOrders(ctx, orderMaxAge)
}

func (o *OrderService) GenerateOrder(ctx context.Context) error {
	seed := rand.NewSource(time.Now().UnixNano())
	rnd := rand.New(seed)

	point := geo.GetRandomAllowedLocation(o.allowedZone, o.disabledZones)
	order := models.Order{
		Price:         math.Round((rnd.Float64()*(maxOrderPrice-minOrderPrice)+minOrderPrice)*100) / 100,
		DeliveryPrice: math.Round((rnd.Float64()*(maxDeliveryPrice-minDeliveryPrice)+minDeliveryPrice)*100) / 100,
		Lng:           point.Lng,
		Lat:           point.Lat,
		IsDelivered:   false,
		CreatedAt:     time.Now(),
	}

	err := o.storage.Save(ctx, order, orderMaxAge)
	if err != nil {
		return err
	}

	return nil
}

func (o *OrderService) Delivery(ctx context.Context, order models.Order) error {
	order.IsDelivered = true

	return o.storage.Update(ctx, order)
}
