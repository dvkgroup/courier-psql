package storage

import (
	"context"
	"fmt"
	"github.com/jmoiron/sqlx"
	geo "github.com/kellydunn/golang-geo"
	"gitlab.com/dvkgroup/courier-psql/module/order/models"
	"time"
)

//go:generate go run github.com/vektra/mockery/v2@v2.20.0 --name OrderStorager
type OrderStorager interface {
	Save(ctx context.Context, order models.Order, maxAge time.Duration) error                       // сохранить заказ с временем жизни
	Update(ctx context.Context, order models.Order) error                                           // сохранить заказ с временем жизни
	GetByID(ctx context.Context, orderID int) (*models.Order, error)                                // получить заказ по id 	// сгенерировать уникальный id
	GetByRadius(ctx context.Context, lng, lat, radius float64, unit string) ([]models.Order, error) // получить заказы в радиусе от точки
	GetCount(ctx context.Context) (int, error)                                                      // получить количество заказов
	RemoveOldOrders(ctx context.Context, maxAge time.Duration) error                                // удалить старые заказы по истечению времени maxAge
}

type OrderStorage struct {
	db *sqlx.DB
}

func NewOrderStorage(db *sqlx.DB) OrderStorager {
	return &OrderStorage{db: db}
}

func (o *OrderStorage) Save(ctx context.Context, order models.Order, maxAge time.Duration) error {
	query := `INSERT INTO orders (price, delivery_price, lng, lat, created_at) 
			  VALUES ($1, $2, $3, $4, $5)`

	_, err := o.db.Exec(query, order.Price, order.DeliveryPrice, order.Lng, order.Lat, time.Now())
	if err != nil {
		return fmt.Errorf("%s", err)
	}

	return nil
}

func (o *OrderStorage) Update(ctx context.Context, order models.Order) error {
	query := `UPDATE orders SET is_delivered=$2 WHERE id=$1`

	_, err := o.db.Exec(query, order.ID, order.IsDelivered)
	if err != nil {
		return fmt.Errorf("%s", err)
	}

	return nil
}

func (o *OrderStorage) RemoveOldOrders(ctx context.Context, maxAge time.Duration) error {
	max := time.Now().Add(-maxAge)
	query := `DELETE FROM orders WHERE created_at < $1`
	_, err := o.db.Exec(query, max)
	if err != nil {
		return err
	}

	return nil
}

func (o *OrderStorage) GetByID(ctx context.Context, orderID int) (*models.Order, error) {
	res := &models.Order{}

	query := `SELECT * FROM orders WHERE id=$1 LIMIT 1`

	err := o.db.Get(res, query, orderID)
	if err != nil {
		return nil, err
	}

	return res, nil
}

func (o *OrderStorage) GetCount(ctx context.Context) (int, error) {
	var id int
	query := "SELECT count(*) FROM orders WHERE is_delivered=false"

	err := o.db.Get(&id, query)
	if err != nil {
		return 0, err
	}

	return id, nil
}

func (o *OrderStorage) GetByRadius(ctx context.Context, lng, lat, radius float64, unit string) ([]models.Order, error) {
	orders := []models.Order{}

	//query := `SELECT id, price, lng, lat, is_delivered, created_at FROM orders`
	query := `SELECT * FROM orders WHERE is_delivered=false`

	err := o.db.Select(&orders, query)
	if err != nil {
		return nil, err
	}

	res := make([]models.Order, 0, len(orders))

	for i := range orders {
		p1 := geo.NewPoint(lat, lng)
		p2 := geo.NewPoint(orders[i].Lat, orders[i].Lng)
		dist := p1.GreatCircleDistance(p2)
		if dist < radius/1000 {
			res = append(res, orders[i])
		}
	}

	return res, nil
}
