package service

import (
	"context"
	cservice "gitlab.com/dvkgroup/courier-psql/module/courier/service"
	cfm "gitlab.com/dvkgroup/courier-psql/module/courierfacade/models"
	oservice "gitlab.com/dvkgroup/courier-psql/module/order/service"
	"log"
)

const (
	CourierVisibilityRadius = 2500 // 2.5km
	CourierDeliveryRadius   = 5    // радиус, в котором защитывается заказ
	RadiusUnitOfMeasure     = "m"  // Can be m, km, ft, or mi.
)

type CourierFacer interface {
	MoveCourier(ctx context.Context, direction, zoom int) // отвечает за движение курьера по карте direction - направление движения, zoom - уровень зума
	GetStatus(ctx context.Context) cfm.CourierStatus      // отвечает за получение статуса курьера и заказов вокруг него
}

// CourierFacade фасад для курьера и заказов вокруг него (для фронта)
type CourierFacade struct {
	courierService cservice.Courierer
	orderService   oservice.Orderer
}

func NewCourierFacade(courierService cservice.Courierer, orderService oservice.Orderer) CourierFacer {
	return &CourierFacade{courierService: courierService, orderService: orderService}
}

func (c CourierFacade) MoveCourier(ctx context.Context, direction, zoom int) {
	courier, err := c.courierService.GetCourier(ctx)
	if err != nil {
		log.Println(err)
		return
	}

	err = c.courierService.MoveCourier(*courier, direction, zoom)
	if err != nil {
		log.Println(err)
	}
}

func (c CourierFacade) GetStatus(ctx context.Context) cfm.CourierStatus {
	courier, err := c.courierService.GetCourier(ctx)
	if err != nil {
		log.Println(err)
		return cfm.CourierStatus{}
	}

	// заказы для отображения
	orders, err := c.orderService.GetByRadius(ctx, courier.Location.Lng, courier.Location.Lat, CourierVisibilityRadius, RadiusUnitOfMeasure)
	if err != nil {
		return cfm.CourierStatus{}
	}

	// оформляем заказ в радиусе CourierDeliveryRadius
	ordersDelivery, err := c.orderService.GetByRadius(ctx, courier.Location.Lng, courier.Location.Lat, CourierDeliveryRadius, RadiusUnitOfMeasure)
	if err != nil {
		return cfm.CourierStatus{}
	}
	for _, o := range ordersDelivery {
		_ = c.orderService.Delivery(ctx, o)
		_ = c.courierService.UpdateScore(*courier, 1)
	}

	return cfm.CourierStatus{
		Courier: *courier,
		Orders:  orders,
	}
}
