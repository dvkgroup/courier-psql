package models

import (
	cm "gitlab.com/dvkgroup/courier-psql/module/courier/models"
	om "gitlab.com/dvkgroup/courier-psql/module/order/models"
)

type CourierStatus struct {
	Courier cm.Courier `json:"courier" db:"courier"`
	Orders  []om.Order `json:"orders" db:"orders"`
}
