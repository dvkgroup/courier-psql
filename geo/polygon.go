package geo

import (
	geo "github.com/kellydunn/golang-geo"
	"math/rand"
	"time"
)

type Point struct {
	Lat float64 `json:"lat"`
	Lng float64 `json:"lng"`
}

//go:generate go run github.com/vektra/mockery/v2@v2.20.0 --name PolygonChecker
type PolygonChecker interface {
	Contains(point Point) bool // проверить, находится ли точка внутри полигона
	Allowed() bool             // разрешено ли входить в полигон
	RandomPoint() Point        // сгенерировать случайную точку внутри полигона
}

func (p Polygon) Contains(point Point) bool {
	return p.polygon.Contains(geo.NewPoint(point.Lat, point.Lng))
}

func (p Polygon) Allowed() bool {
	return p.allowed
}

func (p Polygon) RandomPoint() Point {
	// находим прямоугольнике, в который вмещается полигон
	// далее берем случайную точку из прямоугольника (потом еще нужно её проверить)
	// правильнее было бы через треугольники из вершин полигона
	var minLat, maxLat, minLng, maxLng float64
	for _, p := range p.polygon.Points() {
		if p.Lat() < minLat {
			minLat = p.Lat()
		}
		if p.Lat() > maxLat {
			maxLat = p.Lat()
		}
		if p.Lng() < minLng {
			minLng = p.Lng()
		}
		if p.Lng() > maxLng {
			maxLng = p.Lng()
		}
	}

	seed := rand.NewSource(time.Now().UnixNano())
	rnd := rand.New(seed)

	rndLat := rnd.Float64()*(maxLat-minLat) + minLat
	rndLng := rnd.Float64()*(maxLng-minLng) + minLng

	return Point{rndLat, rndLng}
}

type Polygon struct {
	polygon *geo.Polygon
	allowed bool
}

func NewPolygon(points []Point, allowed bool) *Polygon {
	// используем библиотеку golang-geo для создания полигона
	var geoPoints []*geo.Point

	for _, p := range points {
		geoPoints = append(geoPoints, geo.NewPoint(p.Lat, p.Lng))
	}

	return &Polygon{
		polygon: geo.NewPolygon(geoPoints),
		allowed: allowed,
	}
}

func CheckPointIsAllowed(point Point, allowedZone PolygonChecker, disabledZones []PolygonChecker) bool {
	if allowedZone.Contains(point) {
		for i := range disabledZones {
			if disabledZones[i].Contains(point) {
				return false
			}
		}
		return true
	}
	return false
}

func GetRandomAllowedLocation(allowedZone PolygonChecker, disabledZones []PolygonChecker) Point {
	var point Point
	for {
		point = allowedZone.RandomPoint()

		if CheckPointIsAllowed(point, allowedZone, disabledZones) {
			break
		}
	}
	return point
}

func NewDisAllowedZone1(points []Point) *Polygon {
	// добавить полигон с разрешенной зоной
	// полигоны лежат в /public/js/polygons.js

	return NewPolygon(points, false)
}

func NewDisAllowedZone2(points []Point) *Polygon {
	// добавить полигон с разрешенной зоной
	// полигоны лежат в /public/js/polygons.js

	return NewPolygon(points, false)
}

func NewAllowedZone(points []Point) *Polygon {
	// добавить полигон с разрешенной зоной
	// полигоны лежат в /public/js/polygons.js
	return NewPolygon(points, true)
}
