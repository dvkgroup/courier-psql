package docs

import "gitlab.com/dvkgroup/courier-psql/module/courierfacade/models"

// swagger:route Get /api/status user userListRequest
// Статут курьера
//
// responses:
//	 200: userListResponse

// swagger:response userListResponse
type userListResponse struct {
	// in:body
	Body models.CourierStatus
}
