CREATE TABLE IF NOT EXISTS couriers
(
    id              INTEGER PRIMARY KEY,
    score           INTEGER DEFAULT 0,
    lat             DOUBLE PRECISION,
    lng             DOUBLE PRECISION
);

CREATE TABLE IF NOT EXISTS orders
(
    id              SERIAL PRIMARY KEY,
    price           DECIMAL,
    delivery_price  DECIMAL,
    lat             DOUBLE PRECISION,
    lng             DOUBLE PRECISION,
    is_delivered    BOOL DEFAULT false,
    created_at      TIMESTAMP
);