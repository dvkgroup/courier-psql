#FROM golang:1.19-alpine3.18 AS builder
FROM golang:1.19.4 AS builder
COPY . /go/src/gitlab.com/dvkgroup/courier-psql
WORKDIR /go/src/gitlab.com/dvkgroup/courier-psql
# Create slimest possible image
RUN go build -ldflags="-w -s" -o /go/bin/server /go/src/gitlab.com/dvkgroup/courier-psql/cmd/api

#FROM alpine
FROM golang:1.19.4
# Copy binary from builder
COPY --from=builder /go/bin/server /go/bin/server
COPY ./public /app/public
COPY ./.env /app/.env

WORKDIR /app
# Set entrypoint
ENTRYPOINT ["/go/bin/server"]