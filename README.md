
# Courier PSQL

Если при запуске 
```bash
docker compose up
```
возникает ошибка
> FATAL:  could not open log file "/logs/postgresql.log": Permission denied

нужно дать права на каталог logs
```bash
chmod 777 logs 
```

Права на лог
```bash
sudo chown user:group postgresql.log
```

Отчет
```bash
pgbadger -o pgbadger.html postgresql.log
```